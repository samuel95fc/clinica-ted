package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class Usuario {	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY, generator="seq_usuario")
	private int usuarioId;
	
	@Column(name = "nm_nome")
	private String nome;
	
	@Column(name = "tx_email")
	private String email;
	
	@Column(name = "tx_senha")
	private String senha;
	
	public Integer getUsuarioId() {
		return usuarioId;
	}
	public void setUsuarioId(Integer usuarioId) {
		this.usuarioId = usuarioId;
	}

	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	} 
	
	@Override
    public String toString() {
        return "Usuario [usuarioId=" + usuarioId + ", nome=" + nome
                + ", email=" + email + ", senha=" +senha+"]";
    }    
}