package model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Medico_Especialidade {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	private String nome;
	
	@ManyToOne
	@JoinColumn(name = "medico_id")
	private Medico medico;

	public String getNome() {
		return nome;
	}

	public Medico getMedico() {
		return medico;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setMedico(Medico medico) {
		this.medico = medico;
	}
	
	
	
}
