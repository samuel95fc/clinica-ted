package model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Paciente {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int pacienteId;
	
	@Column(name = "nm_nome")
	private String nome;
	
	@Column(name = "nr_cpf")
	private String cpf;
	
	@Column(name = "nr_rg")
	private String rg;
	
	@ManyToOne(cascade = CascadeType.ALL)
	private Agenda agenda;
	
	

	public Agenda getAgenda() {
		return agenda;
	}
	public void setAgenda(Agenda agenda) {
		this.agenda = agenda;
	}
	public int getPacienteId() {
		return pacienteId;
	}
	public void setPacienteId(int pacienteId) {
		this.pacienteId = pacienteId;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	
	public String getRg() {
		return rg;
	}
	public void setRg(String rg) {
		this.rg = rg;
	}
	
	@Override
    public String toString() {
        return "Paciente [pacienteId=" + pacienteId + ", nome=" + nome + ", cpf=" + cpf + ", rg=" + rg +"]";
    } 
}