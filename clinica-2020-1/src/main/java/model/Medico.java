package model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Medico {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private int medicoId;
	
	@Column(name = "nm_nome")
	private String nome;
	
	@Column(name = "nr_crm")
	private String crm;
	
	@OneToMany(mappedBy = "medico")
	private List<Medico_Especialidade> especialidades = new ArrayList<Medico_Especialidade>();

	public int getMedicoId() {
		return medicoId;
	}
	public void setMedicoId(int medicoId) {
		this.medicoId = medicoId;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getCrm() {
		return crm;
	}
	public void setCrm(String crm) {
		this.crm = crm;
	}
	
	@Override
    public String toString() {
        return "Medico [medicoId=" + medicoId + ", nome=" + nome + ", crm=" + crm + "]";
    } 
}